package tests;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import hospital.Consulta;
import hospital.Doctor;
import hospital.Hospital;

public class AltaConsultaTest {

static Hospital hospital;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		hospital = new Hospital();
	}

	@After
	public void resetearContexto() throws Exception {
		hospital.getListaConsultas().clear();
	
	}
	@Test
	public void testAltaConsultaSinDoctorRegistrado() {
		Doctor doctor = new Doctor();
		doctor.setEspecialidad("traumatologia");
		Consulta consulta = new Consulta(doctor, null, "traumatologia");
		
		hospital.altaConsulta(consulta);
		
		
		boolean actual = hospital.getListaConsultas().contains(consulta);
		
		assertFalse(actual);
	}

	@Test
	public void testAltaConsultaConDoctorRegistrado() {
		Doctor doctor = new Doctor();
		doctor.setEspecialidad("traumatologia");
		hospital.getListaDoctores().add(doctor);
		
		Consulta consulta = new Consulta(doctor, null, "traumatologia");
		
		hospital.altaConsulta(consulta);
		
		boolean actual = hospital.getListaConsultas().contains(consulta);
		
		assertTrue(actual);
	}
	
	@Test
	public void testAltaConsultaDiferenteEspecialidad() {
		Doctor doctor = new Doctor();
		doctor.setEspecialidad("cirugia");
		hospital.getListaDoctores().add(doctor);
		
		Consulta consulta = new Consulta(doctor, null, "traumatologia");
		
		hospital.altaConsulta(consulta);
		
		boolean actual = hospital.getListaConsultas().contains(consulta);
		
		assertFalse(actual);
	}
	
	@Test
	public void testAltaConsultaConOtraConsultaMismaFecha() {
		Doctor doctor = new Doctor();
		doctor.setEspecialidad("cirugia");
		hospital.getListaDoctores().add(doctor);
		Consulta consulta = new Consulta(doctor, null, "cirugia");
		consulta.setFecha(LocalDate.now());
		hospital.getListaConsultas().add(consulta);
		
		Consulta consulta2 = new Consulta(doctor, null, "cirugia");
		consulta2.setFecha(LocalDate.now());
		
		hospital.altaConsulta(consulta);
		
		boolean actual = hospital.getListaConsultas().contains(consulta);
		
		assertFalse(actual);
	}
	
}
