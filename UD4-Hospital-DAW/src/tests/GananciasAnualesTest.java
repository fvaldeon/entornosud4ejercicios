package tests;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import hospital.Consulta;
import hospital.Doctor;
import hospital.Hospital;

public class GananciasAnualesTest {

	static Hospital hospital;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		hospital = new Hospital();
	
	}

	@After
	public void resetearContexto() throws Exception {
		hospital.getListaConsultas().clear();
	
	}

	/*
	 * Prueba positiva criterio 1, prueba negativa criterio 2
	 */
	@Test
	public void testGananciasAnualesSinConsultasEnAnno() {
		Consulta consulta1 = new Consulta(null, null, null);
		consulta1.setFecha(LocalDate.parse("2018-05-04"));
		hospital.getListaConsultas().add(consulta1);
		
		Consulta consulta2 = new Consulta(null, null, null);
		consulta2.setFecha(LocalDate.parse("2017-05-03"));
		hospital.getListaConsultas().add(consulta2);
		
		double esperado = 0.0;
		double actual = hospital.gananciasAnuales("2019");
		
		assertEquals(esperado, actual, 0.0);
	}

	/*
	 * Prueba negativa criterio 1
	 */
	@Test
	public void testGananciasAnualesSinConsultasEnAnnoNegativa() {
		Consulta consulta1 = new Consulta(null, null, null);
		consulta1.setFecha(LocalDate.parse("2018-05-04"));
		hospital.getListaConsultas().add(consulta1);
		
		Consulta consulta2 = new Consulta(null, null, null);
		consulta2.setFecha(LocalDate.parse("2018-05-03"));
		hospital.getListaConsultas().add(consulta2);
		
		double noEsperado = 0.0;
		double actual = hospital.gananciasAnuales("2018");
		
		assertEquals(noEsperado, actual, 0.0);
	}
	
	/*
	 * Prueba positiva criterio2, prueba negativa criterio3
	 */
	@Test
	public void testGananciasAnualesConConsultasEnAnno() {
		Consulta consulta1 = new Consulta(null, null, "Oftalmologia");
		consulta1.setFecha(LocalDate.parse("2018-05-04"));
		hospital.getListaConsultas().add(consulta1);
		
		Consulta consulta2 = new Consulta(null, null, "Medicina General");
		consulta2.setFecha(LocalDate.parse("2018-05-03"));
		hospital.getListaConsultas().add(consulta2);
		
		Consulta consulta3 = new Consulta(null, null, "Podologia");
		consulta3.setFecha(LocalDate.parse("2019-05-03"));
		hospital.getListaConsultas().add(consulta3);
		
		double esperado = 400.0;
		double actual = hospital.gananciasAnuales("2018");
		
		assertEquals(esperado, actual, 0.0);
	}
	
	/*
	 * Prueba positiva criterio 3
	 */
	@Test
	public void testGananciasAnualesConConsultasEspecialidad() {
		Consulta consulta1 = new Consulta(null, null, "Taumatologia");
		consulta1.setFecha(LocalDate.parse("2018-05-04"));
		hospital.getListaConsultas().add(consulta1);
		
		Consulta consulta2 = new Consulta(null, null, "Cirugia");
		consulta2.setFecha(LocalDate.parse("2018-05-03"));
		hospital.getListaConsultas().add(consulta2);
		
		Consulta consulta3 = new Consulta(null, null, "Medicina general");
		consulta3.setFecha(LocalDate.parse("2018-05-03"));
		hospital.getListaConsultas().add(consulta3);
		
		double esperado = 700.0;
		double actual = hospital.gananciasAnuales("2018");
		
		assertEquals(esperado, actual, 0.0);
	}
	
	
	@Test
	public void gastoMensualSinDoctores(){
		double esperado = 0;
		double actual = hospital.gastoMensual();
		
		
		assertEquals(esperado, actual, 0.0);
	}
	
	@Test
	public void gastoMensualConDosDoctores(){
		Doctor doctor = new Doctor();
		hospital.getListaDoctores().add(doctor);
		
		Doctor doctor2 = new Doctor();
		doctor2.setEspecialidad("Cirugia");
		hospital.getListaDoctores().add(doctor2);
		
		double esperado = 4200;
		double actual = hospital.gastoMensual();
		
		
		assertEquals(esperado, actual, 0.0);
	}
	
}
