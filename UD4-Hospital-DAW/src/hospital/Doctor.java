package hospital;

public class Doctor {
	private String codColegiado;
	private String nombre;
	private String especialidad;
	private boolean indefinido;
	
	public Doctor(){
		
	}
	
	public Doctor(String codColegiado, String nombre, String especialidad) {
		this.codColegiado = codColegiado;
		this.nombre = nombre;
		this.especialidad = especialidad;
	}

	public boolean isIndefinido() {
		return indefinido;
	}
	
	public void setIndefinido(boolean indefinido) {
		this.indefinido = indefinido;
	}
	
	public String getCodColegiado() {
		return codColegiado;
	}

	public void setCodColegiado(String codColegiado) {
		this.codColegiado = codColegiado;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public  String getEspecialidad() {
		return especialidad;
	}

	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}
}
