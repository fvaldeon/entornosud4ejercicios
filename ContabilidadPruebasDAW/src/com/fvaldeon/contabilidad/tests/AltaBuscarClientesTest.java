package com.fvaldeon.contabilidad.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import com.fvaldeon.contabilidad.clases.Cliente;
import com.fvaldeon.contabilidad.clases.GestorContabilidad;

public class AltaBuscarClientesTest {

	/*
	 * busco un cliente que no existe en la lista
	 */
	@Test
	public void testBuscarClienteInexistente() {
		GestorContabilidad gestor = new GestorContabilidad();
		
		Cliente actual = gestor.buscarCliente("12352F");
		
		assertNull(actual);
	}
	
	/*
	 * A�ado un cliente a la lista y al buscar otro distinto
	 * no me debe buscar ese
	 */
	@Test
	public void testBuscarClienteInexistenteNotNull() {
		GestorContabilidad gestor = new GestorContabilidad();
		Cliente noEsperado = new Cliente("45765H");
		gestor.getListaClientes().add(noEsperado);
		
		
		Cliente actual = gestor.buscarCliente("12352F");
		
		assertNotEquals(noEsperado, actual);
	}
	
	/*
	 * A�ado un cliente a la lista y le busco
	 */
	@Test
	public void testBuscarClienteExistente() {
		GestorContabilidad gestor = new GestorContabilidad();
		Cliente esperado = new Cliente("1234R");
		gestor.getListaClientes().add(esperado);
		
		Cliente actual = gestor.buscarCliente("1234F");
		
		assertEquals(esperado, actual);
	}
	
	/*
	 * A�ado dos clientes a la lista y busco uno de ellos
	 */
	@Test
	public void testBuscarClienteVariosClientes() {
		GestorContabilidad gestor = new GestorContabilidad();
		gestor.getListaClientes().add(new Cliente("1234R"));
		gestor.getListaClientes().add(new Cliente("4576Y"));
		Cliente esperado = new Cliente("1234R");
		gestor.getListaClientes().add(esperado);
		
		Cliente actual = gestor.buscarCliente("1234F");
		
		assertEquals(esperado, actual);
	}
	
	/*
	 * A�ado un cliente a la lista, y al buscar otro
	 * no de debe devolver el que he a�adido 
	 */
	@Test
	public void testBuscarClienteDistintoNull() {
		//Contexto
		GestorContabilidad gestor = new GestorContabilidad();
		Cliente esperado = new Cliente("1234R");
		gestor.getListaClientes().add(esperado);

		//Prueba del metodo
		Cliente actual = gestor.buscarCliente("1234R");
		
		//Afirmacion
		assertNotNull(actual);
	}

	/*
	 * altaCliente a�ade un cliente, cuando no hay ninguno 
	 */
	@Test
	public void testAltaClienteDniNorepetido(){
		//Contexto
		GestorContabilidad gestor = new GestorContabilidad();
		
		Cliente nuevo = new Cliente("1234R");
		//Prueba del metodo
		gestor.altaCliente(nuevo);
		//Afirmacion
		assertTrue(gestor.getListaClientes().contains(nuevo));
	}
	
	/*
	 * Pruebo que al a�adir un cliente repetido, no se a�ade
	 */
	@Test
	public void testAltaClienteDniRepetido(){
		//Contexto
		GestorContabilidad gestor = new GestorContabilidad();
		gestor.getListaClientes().add(new Cliente("1234R"));
		Cliente repetido = new Cliente("1234R");
		
		//Prueba del metodo
		gestor.altaCliente(repetido);
		
		//Afirmacion
		assertFalse(gestor.getListaClientes().contains(repetido));
	}
	
	/*
	 * Pruebo que al a�adir un cliente con dni no repetido
	 * cuando hay m�s clientes, si se a�ade
	 */
	@Test
	public void testAltaClienteConMasClientes(){
		//Contexto
		GestorContabilidad gestor = new GestorContabilidad();
		gestor.getListaClientes().add(new Cliente("1234R"));
		gestor.getListaClientes().add(new Cliente("6757H"));
		gestor.getListaClientes().add(new Cliente("2345K"));
		Cliente repetido = new Cliente("6757H");
		
		//Prueba del metodo
		gestor.altaCliente(repetido);
		
		//Afirmacion
		assertTrue(gestor.getListaClientes().contains(repetido));
	}
}
