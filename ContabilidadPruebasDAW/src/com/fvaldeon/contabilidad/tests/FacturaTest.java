package com.fvaldeon.contabilidad.tests;

import static org.junit.Assert.*;
import org.junit.Test;


import com.fvaldeon.contabilidad.clases.Factura;

public class FacturaTest {

	@Test
	public void testPrecioTotalCeroProductos() {
		Factura factura = new Factura("2345A");
		factura.setCantidad(0);
		factura.setPrecioUnidad(2.75F);
		
		float actual = factura.calcularPrecioTotal();
		float esperado = 0.0F;
		
		assertEquals(esperado, actual, 0.0);
	}

	@Test
	public void testPrecioTotalConPrecioCero() {
		Factura factura = new Factura("2345A");
		factura.setCantidad(2);
		factura.setPrecioUnidad(0.0F);
		
		float actual = factura.calcularPrecioTotal();
		float esperado = 0.0F;
		
		assertEquals(esperado, actual, 0.0);
	}
	
	@Test
	public void testPrecioTotalConPrecioDistintoCero() {
		Factura factura = new Factura("2345A");
		factura.setCantidad(2);
		factura.setPrecioUnidad(2.75F);
		
		float actual = factura.calcularPrecioTotal();
		float esperado = 5.5F;
		
		assertEquals(esperado, actual, 0.0);
	}
}
