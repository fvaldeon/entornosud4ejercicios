package com.fvaldeon.contabilidad.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import com.fvaldeon.contabilidad.clases.Cliente;
import com.fvaldeon.contabilidad.clases.GestorContabilidad;

public class GestorContabilidadClienteTest {

	@Test
	public void testBuscarClienteInexistente() {
		GestorContabilidad gestor = new GestorContabilidad();
		
		Cliente actual = gestor.buscarCliente("12352F");
		
		assertNull(actual);
	}
	
	@Test
	public void testBuscarClienteInexistenteNotNull() {
		GestorContabilidad gestor = new GestorContabilidad();
		Cliente noEsperado = new Cliente("45765H");
		gestor.getListaClientes().add(noEsperado);
		
		
		Cliente actual = gestor.buscarCliente("12352F");
		
		assertNotEquals(noEsperado, actual);
	}
	
	@Test
	public void testBuscarClienteExistente() {
		GestorContabilidad gestor = new GestorContabilidad();
		Cliente esperado = new Cliente("1234R");
		gestor.getListaClientes().add(esperado);
		
		Cliente actual = gestor.buscarCliente("1234F");
		
		assertEquals(esperado, actual);
	}
	
	@Test
	public void testBuscarClienteVariosClientes() {
		GestorContabilidad gestor = new GestorContabilidad();
		gestor.getListaClientes().add(new Cliente("1234R"));
		gestor.getListaClientes().add(new Cliente("1234R"));
		Cliente esperado = new Cliente("1234R");
		gestor.getListaClientes().add(esperado);
		
		Cliente actual = gestor.buscarCliente("1234F");
		
		assertEquals(esperado, actual);
	}
	
	@Test
	public void testBuscarClienteDistintoNull() {
		GestorContabilidad gestor = new GestorContabilidad();
		Cliente esperado = new Cliente("1234R");
		gestor.getListaClientes().add(esperado);
		
		Cliente actual = gestor.buscarCliente("1234F");
		
		assertNotNull(actual);
	}

}
