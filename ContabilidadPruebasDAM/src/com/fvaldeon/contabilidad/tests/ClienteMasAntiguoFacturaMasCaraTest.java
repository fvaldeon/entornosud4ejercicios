package com.fvaldeon.contabilidad.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fvaldeon.contabilidad.clases.Cliente;
import com.fvaldeon.contabilidad.clases.GestorContabilidad;

public class ClienteMasAntiguoFacturaMasCaraTest {

static GestorContabilidad gestor;
	
	@BeforeClass
	public static void setUpBeforeClass() {
		gestor = new GestorContabilidad();
	}
	
	
	@Before
	public void setUpBeforeTest(){
		gestor.getListaClientes().clear();
		gestor.getListaFacturas().clear();
	}
	
	
	@Test 
	public void testClienteMasAntiguoCeroClientes(){
		Cliente actual = gestor.clienteMasAntiguo();
		
		assertNull(actual);
	}
	
	//Prueba negativa del criterio que devuelve null
	@Test 
	public void testClienteMasAntiguoConClientes(){
		Cliente cliente1 = new Cliente("4576U");
		cliente1.setFechaAlta(LocalDate.now());
		gestor.getListaClientes().add(cliente1);
		
		Cliente actual = gestor.clienteMasAntiguo();
		
		assertNotNull(actual);
	}
	
	@Test
	public void testClienetMasAntiguoCon3Clientes(){
		Cliente cliente1 = new Cliente("4576U");
		cliente1.setFechaAlta(LocalDate.now());
		gestor.getListaClientes().add(cliente1);
		Cliente esperado = new Cliente("42345G");
		esperado.setFechaAlta(LocalDate.parse("2015-04-06"));
		gestor.getListaClientes().add(esperado);
		Cliente cliente3 = new Cliente("4576U");
		cliente3.setFechaAlta(LocalDate.parse("2016-04-06"));
		gestor.getListaClientes().add(cliente3);
		
		
		Cliente actual = gestor.clienteMasAntiguo();
		
		assertEquals(esperado, actual);
	}
	
	@Test
	public void testClienetMasAntiguoCon3ClientesNegativa(){
		Cliente noEsperado = new Cliente("4576U");
		noEsperado.setFechaAlta(LocalDate.now());
		gestor.getListaClientes().add(noEsperado);
		Cliente cliente2 = new Cliente("42345G");
		cliente2.setFechaAlta(LocalDate.parse("2015-04-06"));
		gestor.getListaClientes().add(cliente2);

		Cliente actual = gestor.clienteMasAntiguo();
		assertNotEquals(noEsperado, actual);
	}
	
}
