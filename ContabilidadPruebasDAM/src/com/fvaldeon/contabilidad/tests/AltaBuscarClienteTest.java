package com.fvaldeon.contabilidad.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fvaldeon.contabilidad.clases.Cliente;
import com.fvaldeon.contabilidad.clases.GestorContabilidad;

public class AltaBuscarClienteTest {

	static GestorContabilidad gestor;
	
	@BeforeClass
	public static void setUpBeforeClass() {
		gestor = new GestorContabilidad();
	}
	
	
	@Before
	public void setUpBeforeTest(){
		gestor.getListaClientes().clear();
		gestor.getListaFacturas().clear();
	}
	
	@Test
	public void testBuscarClienteInexistente() {
		Cliente actual = gestor.buscarCliente("1234R");
		assertNull(actual);
	}
	
	@Test
	public void testBuscarClienteInexistenteConClientes() {
		gestor.getListaClientes().add(new Cliente("5423G"));
		gestor.getListaClientes().add(new Cliente("567868L"));
				
		Cliente actual = gestor.buscarCliente("1234R");
		assertNull(actual);
	}
	
	@Test
	public void testNegativoBuscarClienteInexistenteConClientes() {
		gestor.getListaClientes().add(new Cliente("5423G"));
		gestor.getListaClientes().add(new Cliente("567868L"));
		
		Cliente actual = gestor.buscarCliente("5423G");
		assertNotNull(actual);
	}
	
	@Test
	public void testBuscarClienteExistenteEntreDos() {
		Cliente esperado = new Cliente("1234G");
		gestor.getListaClientes().add(esperado);
		Cliente cliente2 = new Cliente("45787H");
		gestor.getListaClientes().add(cliente2);
				
		Cliente actual = gestor.buscarCliente("1234G");
				
		assertEquals(esperado, actual);
	}
	
	@Test
	public void testNegativoBuscarClienteExistenteEntreDos() {
		Cliente cliente1 = new Cliente("1234G");
		gestor.getListaClientes().add(cliente1);
		Cliente noEsperado = new Cliente("45787H");
		gestor.getListaClientes().add(noEsperado);
				
		Cliente actual = gestor.buscarCliente("1234G");
				
		assertNotEquals(noEsperado, actual);
	}
	
	
	@Test
	public void altaClienteConCeroClientes(){
		Cliente esperado = new Cliente("1234R");
		gestor.altaCliente(esperado);
		
		assertTrue(gestor.getListaClientes().contains(esperado));
	}
	
	@Test
	public void altaClienteConVariosClientesAnadidos(){
		Cliente cliente1 = new Cliente("4576U");
		gestor.getListaClientes().add(cliente1);
		Cliente cliente2 = new Cliente("42345G");
		gestor.getListaClientes().add(cliente2);
		
		Cliente esperado = new Cliente("1234R");
		gestor.altaCliente(esperado);
		
		assertTrue(gestor.getListaClientes().contains(esperado));
	}
	
	
	@Test
	public void altaClienteNegativa(){
		Cliente cliente1 = new Cliente("4576U");
		gestor.getListaClientes().add(cliente1);
		Cliente cliente2 = new Cliente("42345G");
		gestor.getListaClientes().add(cliente2);
		
		Cliente esperado = new Cliente("4576U");
		gestor.altaCliente(esperado);
		
		boolean resultado = gestor.getListaClientes().contains(esperado);
		
		assertFalse(resultado);
	}
	
	
	
}
