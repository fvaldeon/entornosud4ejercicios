package com.fvaldeon.contabilidad.clases;

import java.util.ArrayList;

public class GestorContabilidad {
	
	private ArrayList<Factura> listaFacturas;
	private ArrayList<Cliente> listaClientes;
	
	public GestorContabilidad() {
		listaFacturas = new ArrayList<>();
		listaClientes = new ArrayList<>();
	}

	public ArrayList<Factura> getListaFacturas() {
		return listaFacturas;
	}

	public ArrayList<Cliente> getListaClientes() {
		return listaClientes;
	}
	
	public void altaCliente(Cliente cliente){
		String dni = cliente.getDni();
		for(Cliente client : listaClientes){
			if(client.getDni().equals(dni)){
				return;
			}
		}
		listaClientes.add(cliente);
	}
	
	//Sin implementar
	public Cliente buscarCliente(String dni){
		for(Cliente cliente : listaClientes){
			if(cliente.getDni().equals(dni)){
				return cliente;
			}
		}
		return null;
	}
	
	public Factura buscarFactura(String codigo){
		return null;
	}
	
	public void crearFactura(Factura factura){
		
	}
	
	public Cliente clienteMasAntiguo(){
		
		return null;
	}
	
	public Factura facturaMasCara(){
		return null;
	}
	
	public float calcularFacturacionAnual(int anno){
		return 0;
	}
	
	public void asignarClienteAFactura(String dni, String codigoFactura){
		
	}
	
	public int cantidadFacturasPorCliente(String dni){
		return 0;
	}
	
	public void eliminarFactura(String c�digo){
		
	}
	
	public void eliminarCliente(String dni){
		
	}
}
