package hospital;

import java.time.LocalDate;

public class Consulta {
	
	private static int generadorCodConsulta;
	
	private int codconsulta;
	private Doctor doctor;
	private Paciente paciente;
	private LocalDate fecha;
	private String especialidad;
	
	public Consulta(Doctor doctor, Paciente paciente, String especialidad) {
		//Se le asigna un id distinto a cada consulta de forma automática
		this.codconsulta = ++generadorCodConsulta;

		//Se asigna por defecto la fecha actual
		this.fecha = LocalDate.now();

		this.doctor = doctor;
		this.paciente = paciente;
		this.especialidad = especialidad;
	}

	public int getCodconsulta() {
		return codconsulta;
	}

	public Doctor getDoctor() {
		return doctor;
	}

	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public LocalDate getFecha() {
		return fecha;
	}
	
	public void setFecha(LocalDate fecha){
		this.fecha = fecha;
	}

	public String getEspecialidad() {
		return especialidad;
	}

	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}
	
	
}
