package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import hospital.Doctor;
import hospital.Hospital;

public class TestEliminarDoctor {

	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	
	
	}

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testEliminarDoctor() {
		Hospital hospital = new Hospital();
		
		Doctor doctor1 = new Doctor();
		doctor1.setCodColegiado("12");
		hospital.getListaDoctores().add(doctor1);
		
		Doctor doctor2 = new Doctor();
		doctor2.setCodColegiado("13");
		hospital.getListaDoctores().add(doctor2);
		
		hospital.eliminarDoctor("15");
		
		boolean condicion1 = hospital.getListaDoctores().contains(doctor1);
		boolean condicion2 = hospital.getListaDoctores().contains(doctor2);
		
		assertTrue(condicion1 && condicion2);
	}
	


}
