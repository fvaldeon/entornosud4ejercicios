package tests;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import hospital.Consulta;
import hospital.Hospital;

public class TestGananciasAnuales {

	static Hospital hospital;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		hospital = new Hospital();
	}

	@AfterClass
	public static void terminarClasePruebas() throws Exception {
		hospital = null;
	}
	
	@After
	public void resetearDespuesDeCadaMetodo() throws Exception {
		hospital.getListaConsultas().clear();
	}

	@Test
	public void testGananciasAnualesSinConsultasEsteAnno() {
		Consulta consulta = new Consulta(null, null, null);
		consulta.setFecha(LocalDate.parse("2019-01-01"));
		hospital.getListaConsultas().add(consulta);
		
		double esperado = 0;
		double actual = hospital.gananciasAnuales("2018");
		
		assertEquals(esperado, actual, 0.0);
	}
	
	@Test
	public void testGananciasAnualesConConsultas() {
		Consulta consulta = new Consulta(null, null, null);
		consulta.setFecha(LocalDate.parse("2019-01-01"));
		hospital.getListaConsultas().add(consulta);
		Consulta consulta2 = new Consulta(null, null, null);
		hospital.getListaConsultas().add(consulta2);
		consulta.setFecha(LocalDate.parse("2018-01-01"));
		Consulta consulta3 = new Consulta(null, null, null);
		consulta.setFecha(LocalDate.parse("2019-05-06"));
		hospital.getListaConsultas().add(consulta3);
		
		double esperado = 400;
		double actual = hospital.gananciasAnuales("2019");
		
		assertEquals(esperado, actual, 0.0);
	}
	
	@Test
	public void testGananciasAnualesConConsultasNegativa() {
		
		Consulta consulta = new Consulta(null, null, null);
		consulta.setFecha(LocalDate.parse("2019-01-01"));
		hospital.getListaConsultas().add(consulta);
		Consulta consulta2 = new Consulta(null, null, null);
		consulta.setFecha(LocalDate.parse("2018-01-01"));
		hospital.getListaConsultas().add(consulta2);
		Consulta consulta3 = new Consulta(null, null, null);
		consulta.setFecha(LocalDate.parse("2019-05-06"));
		hospital.getListaConsultas().add(consulta3);
		
		double noEsperado = 300;
		double actual = hospital.gananciasAnuales("2019");
		
		assertNotEquals(noEsperado, actual);
	}
	
	@Test
	public void testGananciasAnualesConEspecialidades() {
		Consulta consulta = new Consulta(null, null, "Cirugia");
		consulta.setFecha(LocalDate.parse("2019-01-01"));
		hospital.getListaConsultas().add(consulta);
		
		Consulta consulta2 = new Consulta(null, null, "Traumatologia");
		consulta.setFecha(LocalDate.parse("2019-02-01"));
		hospital.getListaConsultas().add(consulta2);
		
		Consulta consulta3 = new Consulta(null, null, "Oftalmologia");
		consulta.setFecha(LocalDate.parse("2019-05-06"));
		hospital.getListaConsultas().add(consulta3);
	
		double esperado = 700;
		double actual = hospital.gananciasAnuales("2019");
		
		assertEquals(esperado, actual, 0.0);
	}
	
	@Test
	public void testGananciasAnualesConEspecialidadesNegativa() {
		Consulta consulta = new Consulta(null, null, "Cirugia");
		consulta.setFecha(LocalDate.parse("2019-01-01"));
		hospital.getListaConsultas().add(consulta);
		
		Consulta consulta2 = new Consulta(null, null, "Traumatologia");
		consulta.setFecha(LocalDate.parse("2019-02-01"));
		hospital.getListaConsultas().add(consulta2);
		
		Consulta consulta3 = new Consulta(null, null, "Oftalmologia");
		consulta.setFecha(LocalDate.parse("2019-05-06"));
		hospital.getListaConsultas().add(consulta3);
	
		double noEsperado = 600;
		double actual = hospital.gananciasAnuales("2019");
		
		assertEquals(noEsperado, actual, 0.0);
	}

}
